**Automatización mobile appium para Eribank**

Para éste reto se usa la herramienta de appium para hacer la integración del desarrollo de la automatización y el dispositivo mobile sea simulador o físico.

*Se debe tener en cuenta que se usa un archivo llamada data.xlsx el cual representa el datadriven o el set de datos que serán cargado en la automatización.*

---

## Patrón de automatización Screenplay

Para éste ejercicio se usa el patrón de automatización Screenplay, también se utiliza la capa model para la transferencia de objetos de datos.

---

## Clases generadas

Se crean clases externas para que éstas sean reutilizadas en la codificación y así se reduzca la cantidad de líneas empleadas en el mismo.

---

## Datadriven Excel

Se logra implementar un pequeño set de datos cargado desde un archivo de Excel para evitar quemar en el código los datos que serán ejecutados en el formulario de registro.

---