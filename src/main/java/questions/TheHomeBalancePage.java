package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import userinterface.HomeLauncher;

public class TheHomeBalancePage implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return HomeLauncher.BOTON_SALDO.resolveFor(actor).isVisible();
    }

    public static TheHomeBalancePage isVisible() {
        return new TheHomeBalancePage();
    }

}
