package tasks;

import model.Usuario;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.LoginLauncher;

import java.util.List;

public class Login implements Task {

    private List<Usuario> data;

    public Login(List<Usuario> data) {
        this.data = data;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
           actor.attemptsTo(Enter.theValue(data.get(0).getUser()).into(LoginLauncher.CAMPO_USUARIO));
           actor.attemptsTo(Enter.theValue(data.get(0).getPass()).into(LoginLauncher.CAMPO_CLAVE));
           actor.attemptsTo(Click.on(LoginLauncher.BOTON_LOGIN));
    }

    public static Login inEribank(List<Usuario> data) {

        return Tasks.instrumented(Login.class, data);
    }
}
